package hu.dpc.edu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void beforeEach() {
        calculator = new Calculator();
    }

    @Nested
    @DisplayName("Initially")
    class Initially {
        @Test
        @DisplayName("Initially 0 should be displayed")
        public void initialState() {
            assertEquals("0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("When pressing a number, it should replace the 0")
        public void pressingNumber() {
            calculator.numberButtonPressed(3);
            assertEquals("3", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("When pressing the decimal separator, it should display '0.'")
        public void decimalSeparatorInitially() {
            calculator.decimalSeparatorPressed();
            assertEquals("0.", calculator.getDisplayedText());
        }

    }

    @Nested
    @DisplayName("During entering the first number")
    class FirstNumber {
        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(2);
            calculator.numberButtonPressed(9);
        }

        @Test
        @DisplayName("When decimal separator pressed it should be simply appended")
        public void pressingSecondNumber() {
            calculator.decimalSeparatorPressed();
            assertEquals("29.", calculator.getDisplayedText());
        }

        @Nested
        @DisplayName("After decimal sign was entered")
        class FirstNumberDecimal {
            @BeforeEach
            void beforeEach() {
                calculator.decimalSeparatorPressed();
            }

            @Test
            @DisplayName("A second decimal separator does nothing")
            public void pressingSecondNumber() {
                calculator.decimalSeparatorPressed();
                assertEquals("29.", calculator.getDisplayedText());
            }

            @Test
            @DisplayName("A second decimal separator does nothing even after more numbers")
            public void pressingSecondNumberAfterNumbers() {
                calculator.numberButtonPressed(7);
                calculator.numberButtonPressed(6);
                calculator.decimalSeparatorPressed();
                assertEquals("29.76", calculator.getDisplayedText());
            }

        }
    }

    @Nested
    @DisplayName("After first integer is entered")
    class AfterFirstNumber {
        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(3);
            calculator.numberButtonPressed(4);
        }

        @Nested
        @DisplayName("And Add button is pressed")
        class AddButtonPressed {
            @BeforeEach
            void beforeEach() {
                calculator.addButtonPressed();
            }

            @Test
            @DisplayName("displayedText remains unchanged")
            public void pressingSecondNumber() {
                assertEquals("34", calculator.getDisplayedText());
            }

            @Test
            @DisplayName("Pressing a number button replaced the displayedText")
            public void pressingSecondNumberAfterNumbers() {
                calculator.numberButtonPressed(7);
                assertEquals("7", calculator.getDisplayedText());
            }

        }


    }

}