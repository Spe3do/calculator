package hu.dpc.edu;

@FunctionalInterface
public interface Operator {
    public double evaluate(double firstOperand, double secondOperand);
}
