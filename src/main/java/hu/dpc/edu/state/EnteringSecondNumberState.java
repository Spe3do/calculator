package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class EnteringSecondNumberState extends CalculatorState {

    @Override
    public void onEnter(Calculator calculator) {
        //do nothing
    }

    @Override
    public void operatorPressed(Calculator calculator, Operator operator) {

    }

    @Override
    public void numberButtonPressed(Calculator calculator, String num) {
        calculator.appendToDisplayedText(num);
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        //do nothing
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.appendToDisplayedText(".");
        calculator.setState(new EnteringSecondNumberDecimalPartState());
    }

    @Override
    public String toString() {
        return "Entering sencond number";
    }
}
